import numpy as np

def matrix_to_init_sys(name, data):
	if data is None:
		data = np.matrix([[0]])

	data = np.asarray(data)

	if 0 in data.shape:
		data = np.array([[0]])

	values = ",".join(map(str, data.flatten().tolist()))
	size = data.shape

	if not len(size):
		size = (1,1)

	return f"const float __init_{name}[{size[0]*size[1]}] = " + "{" + values + "};\n"

def matrix_to_init_cosim(name, data):
	if data is None or 0 in data.shape:
		data = np.matrix([[0]])

	data_tr = np.transpose(data)
	values = []

	for i in range(data_tr.shape[0]):
		values.append("{" + ",".join(map(str, data_tr[i])) + "}")

	return f"const std::vector<std::vector<float>> __cosim_{name} = " + "{" + ",".join(values) + "};\n"
