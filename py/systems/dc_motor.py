import numpy as np

ts = 0.004
T = 0.06
K = 0.15

# System definition

A = np.matrix([
	[1, ts],
	[0, 1 - ts/T]
])

B = np.matrix([
	[0],
	[ts*(K/T)]
])

C = None

# Cost function

Gamma = 0.24

Omega = np.matrix([
	[11000,  0],
	[    0, 29]
])

OmegaN = Omega

# Reference

y_ref = None

# Constraints

u_min = -100
u_max = 100

x_min = None
x_max = None

Nx_min = None
Nx_max = None
