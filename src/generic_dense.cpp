#include <iostream>
#include <chrono>

#include "mpc/mpc_dense.hpp"
#include "mpc/generic_dense_defaults.hpp"
#include "mpc/generic_dense_init.hpp"

/*!
@file   generic_dense.cpp
*/

/*!
@brief  Simulates the run of a generic system in dense form.
        The MPC input matrices must be given through Stdin
        The MPC output values will be showed in prompt
        Also, duration time of interation will be showed in prompt too.
        The outputs are given every iteration
        Is highly recommended to run the examples from python sources.
*/

constexpr int N = MPC_N;
constexpr int M = MPC_M;
constexpr int P = MPC_P;
constexpr int L = MPC_L;
constexpr int V = MPC_V;

constexpr Solvers SOLVER = MPC_SOLVER;
constexpr MpcConstraints CONSTRAINTS = MPC_CONSTRAINTS;
constexpr bool TRACK_REF = MPC_TRACK_REF;
constexpr int QP_ITER = MPC_QP_ITER;
constexpr int TOL = MPC_TOL;

int main()
{
	uint32_t num_iters = 0;
	std::cin >> num_iters;

	auto A = Matrix<N,N>(__init_A);
	auto B = Matrix<N,M>(__init_B);
	auto AL = A.pow(L);

	auto Acal = Matrix<N*L,N>(__init_Acal);
	auto Hcal = Matrix<M*L,M*L>(__init_Hcal);
	auto h_base = Matrix<M*L,N>(__init_h_base);

	auto Mx = Matrix<V,M*L>(__init_Mx);
	auto umin = Matrix<M,1>(__init_umin);
	auto umax = Matrix<M,1>(__init_umax);
	auto xmin = Matrix<N,1>(__init_xmin);
	auto xmax = Matrix<N,1>(__init_xmax);
	auto Nxmin = Matrix<N,1>(__init_Nxmin);
	auto Nxmax = Matrix<N,1>(__init_Nxmax);
	auto cx = Matrix<V,1>(__init_cx);

#if MPC_TRACK_REF
	auto Lx = Matrix<N,P>(__init_Lx);
	auto Lu = Matrix<M,P>(__init_Lu);
	auto y_ref = Matrix<P,1>(0.0);
#endif

	auto xinfy = Matrix<N,1>(0.0);
	auto uinfy = Matrix<M,1>(0.0);

	auto u = Matrix<M,1>(0.0);
	auto x = Matrix<N,1>::loadFromStdin();

	// Run the test

	while(--num_iters)
	{
		// Stationary values

#if MPC_TRACK_REF
		y_ref = Matrix<P,1>::loadFromStdin();

		xinfy = Lx * y_ref;
		uinfy = Lu * y_ref;
#endif

		// Run controller

		auto t0 = std::chrono::high_resolution_clock::now();

		mpc_dense<SOLVER, CONSTRAINTS, L, TRACK_REF, QP_ITER, TOL>(
			AL,
			Acal, Hcal, Mx,
			umin, umax, uinfy,
			xmin, xmax, xinfy,
			Nxmin, Nxmax,
			h_base,
			cx, x, u
		);

		auto t1 = std::chrono::high_resolution_clock::now();

		// Simulate system response

		x = A * x + B * u;

		// Print metrics

		std::cout << "X ";
		x.printLine();

		std::cout << "U ";
		u.printLine();

		std::cout << "T " << std::chrono::duration_cast<std::chrono::nanoseconds>(t1 - t0).count() << std::endl;
	}

	return EXIT_SUCCESS;
}
